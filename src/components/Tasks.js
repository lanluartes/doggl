import React, { Component } from 'react';
import Task from './Task'
import AddTask from './AddTask'
import Card from './Modal'
import AlertDismissable from './Alert'

const axios = require('axios')

class Tasks extends Component {

    constructor(props) {
        super(props)

        this.state = {
            tasks: [],
            show: false,
            currTask: {},
            isUpdate: false,
            alertShow: false
        }
    }

    componentDidMount() {
        //work on getting the data from database, then returning it as something draggable?
        this.getData()
    }

    getData = () => {
        axios.get('/api/tasks')
            .then(res => {
                this.setState({ tasks: res.data })
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    renderTasks = () => {
        return (
            <div className="tasks">
                {
                    this.state.tasks.map((task) =>
                        (<Task id={task.TaskID} value={task.TaskTitle} key={task.TaskID} taskGoal={task.TaskGoal} isDone={task.isDone} task={task} setCurrTask={this.setCurrTask} />)
                    )
                }
            </div>
        );
    }

    handleClose = () => {
        this.setState({ show: false, isUpdate: false })
    }

    handleShow = () => {
        this.setState({ show: true })
    }

    handleAlertShow = () => {
        //sets the alert to visible
        this.setState({ alertShow: true })
    }

    handleAlertHide = () => {
        this.setState({ alertShow: false })
    }

    setCurrTask = (data) => {
        this.setState({ currTask: data, isUpdate: true }, () => {
            this.handleShow()
        })

    }

    confirmDelete = () => {
        axios({
            method: 'POST',
            url: '/api/deleteTask',
            data: this.state.currTask
        })
            .then(() => {
                this.getData()
                this.handleAlertHide()
            })
            .catch(err => {
                console.log(err)
            })
    }

    handleDelete = () => {
        //close the modal
        this.handleClose()

        //show the alert
        this.handleAlertShow()
    }

    render() {
        return (
            <div>
                <AlertDismissable show={this.state.alertShow} handleShow={this.handleAlertShow} handleClose={this.handleAlertHide} confirmDelete={this.confirmDelete} task={this.state.currTask} />
                <h2>Tasks</h2>

                {this.renderTasks()}


                <Card isVisible={this.state.show} handleClose={this.handleClose} getData={this.getData} isUpdate={this.state.isUpdate} task={this.state.currTask} handleDelete={this.handleDelete} />

                <AddTask getData={this.getData} handleShow={this.handleShow} />
            </div>
        );
    }
}

export default Tasks;