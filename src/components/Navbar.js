import React, { Component } from 'react'

export default class Navbar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isScrolled: false,
            stat: "nav-bar",
            theme: 'light'
        }
    }

    componentDidMount() {
        document.addEventListener('scroll', this.trackScrolling);

    }

    componentWillUnmount() {
        document.removeEventListener('scroll', this.trackScrolling);
    }

    trackScrolling = () => {
        if (this.state.stat !== "sticky") {
            this.setState({ stat: "sticky" })
        }

        else if (window.pageYOffset === 0) {
            this.makeNav()
        }
        console.log(this.state.stat)
    }

    makeNav = () => {
        if (this.state.stat !== "nav-bar") {
            this.setState({ stat: "nav-bar" })
        }
    }

    goHome = () => {
        window.scrollTo(0, 0)
    }

    handleChange = (event) => {
        const theme = this.state.theme === 'dark' ? 'light' : 'dark';
        this.setState({ theme: theme });
        document.documentElement.setAttribute("data-theme", theme);
    }

    render() {
        let style = "nav-component"
        if (this.state.stat === "sticky") {
            style = "sticky-component"
        }

        return (
            <nav className={this.state.stat}>
                <div className="main-nav-container">
                    <div className="home-container">
                        <p className={style} style={{ float: "left", margin: 0 }}>
                            doggl
                        </p>
                    </div>

                    <div id="nav-container">
                        <input type="checkbox" id="switch" onChange={this.handleChange} /><label htmlFor="switch">Toggle</label>
                    </div>

                </div>
            </nav>
        )
    }
}
