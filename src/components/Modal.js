import React, { Component } from 'react';
import { Modal, Button, InputGroup, FormControl, Form } from 'react-bootstrap'

const axios = require('axios');


export default class Card extends Component {

    constructor(props) {
        super(props);

        this.state = {
            task: {
                TaskID: undefined,
                TaskTitle: "",
                isDone: false,
                TaskGoal: "",
            },
            validated: false
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);

    }

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        let { task } = this.state;
        task[name] = value;

        this.setState({ [name]: value });

    }

    handleSubmit(event) {

        event.preventDefault();
        event.stopPropagation();

        if (!event.target.checkValidity()) {
            // form is invalid! so we do nothing
            return;
        }

        this.setState({ validated: true });

        // add task function
        if (this.props.isUpdate === true) {

            axios({
                method: 'POST',
                url: '/api/editTask',
                data: this.state.task
            })
                .then(() => {
                    this.props.getData()
                    this.props.handleClose()
                })
                .catch(err => {
                    console.log(err)
                })

        } else {
            axios({
                method: 'POST',
                url: '/api/addTask',
                data: this.state.task
            })
                .then(() => {
                    this.props.getData()
                    this.props.handleClose()
                })
                .catch(err => {
                    console.log(err)
                })
        }

    }

    checkIfUpdate = () => {
        if (this.props.isUpdate === true) {
            let { task } = this.props;
            this.setState({ task })
        } else {
            let task = {
                TaskID: undefined,
                TaskTitle: "",
                isDone: false,
                TaskGoal: "",
            }
            this.setState({ task })
        }
    }

    render() {
        const { validated } = this.state
        return (

            <Modal
                show={this.props.isVisible}
                onShow={() => { this.setState({ validated: false }, this.checkIfUpdate()) }}
                onHide={() => { this.props.handleClose() }}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >

                <Form
                    noValidate
                    validated={validated}
                    onSubmit={this.handleSubmit}
                >
                    <Modal.Header closeButton>

                        <InputGroup size="lg">

                            <InputGroup.Prepend>
                                <InputGroup.Text id="inputGroup-sizing-lg">Task</InputGroup.Text>
                            </InputGroup.Prepend>

                            <FormControl aria-label="Large" name="TaskTitle" aria-describedby="inputGroup-sizing-sm" placeholder="Get the job done! 👍" required onChange={this.handleInputChange} value={this.state.task.TaskTitle} isValid={this.state.validated} />

                        </InputGroup>

                    </Modal.Header>

                    <Modal.Body>

                        <InputGroup className="mb-3">

                            <InputGroup.Prepend>
                                <InputGroup.Checkbox aria-label="Checkbox for following text input" name="isDone" checked={this.state.task.isDone} onChange={this.handleInputChange} />
                            </InputGroup.Prepend>

                            <FormControl aria-label="Text input with checkbox" placeholder="Small goals for task" name="TaskGoal" onChange={this.handleInputChange} value={this.state.task.TaskGoal} />

                        </InputGroup>

                        {/* 
                        <Button variant="primary" size="lg" block onClick={() => this.addGoal()}>
                            +
                        </Button> */}

                    </Modal.Body>

                    <Modal.Footer>


                        <Button variant="danger" type="button" onClick={() => this.props.handleDelete()} disabled={!this.props.isUpdate}>
                            Delete
                        </Button>

                        <Button variant="primary" type="submit">
                            Save Changes
                        </Button>

                    </Modal.Footer>
                </Form>

            </Modal >


        )
    }
}