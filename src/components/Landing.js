import React from 'react';

function Landing() {
    return (
        <div className="App">
            <header className="App-header">
                <h1>
                    doggl
                </h1>

                <h3>
                    Track how much time you've spent on a task.<span role="img" aria-label="checkmark">️️✔️</span>
                </h3>

                <p>Get the job done now — or later. <span role="img" aria-label="laughing-emoji">😂</span></p>

                <button>
                    Sign in/Sign up
                </button>
            </header>
        </div>
    );
}

export default Landing;
