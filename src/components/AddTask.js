import React, { Component } from 'react';

export default class AddTask extends Component {

    constructor(props) {
        super(props);
        this.state = {
            items: [],
        };
    }


    render() {
        return (
            <div className="add-task">
                <button className="add-button" onClick={() => this.props.handleShow()}>
                    +
                </button>
            </div>
        );
    };
}

