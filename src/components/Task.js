import React, { Component } from 'react';

//data from the database should be requested from the server

class Task extends Component {

    showMessage = () => {
        console.log(this.props, "props from task")
    }

    render() {
        return (
            <div className="task" onClick={() => this.props.setCurrTask(this.props.task)}>
                <p>{this.props.value}</p>
                {/* <button >show data</button> */}
            </div>
        )
    }

}

export default Task;