import React from 'react';
import { Button, Alert } from 'react-bootstrap'


export default class AlertDismissible extends React.Component {

    render() {
        return (
            <Alert show={this.props.show} variant="warning">
                <Alert.Heading>Warning!</Alert.Heading>
                <p>
                    Are you sure you want to delete?
                </p>
                <hr />
                <div className="d-flex justify-content-end">
                    <Button onClick={() => this.props.handleClose()} variant="warning">
                        Cancel
                    </Button>

                    <Button onClick={() => this.props.confirmDelete(this.props.task)} variant="danger">
                        Delete
                    </Button>
                </div>
            </Alert>
        );
    }
}