var mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 's3cr3t',
    database: 'todo'
})

connection.on('error', err => {
    if (err) {
        console.log('Cannot connect to the database - ', err.code);
    }
})

exports.connection = function () {
    return connection;
};