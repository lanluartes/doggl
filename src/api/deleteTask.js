var express = require('express');
var router = express.Router();
var db = require('../database/connection');

var connection = db.connection(); //connection to database

router.post('/deleteTask', (req, res) => {

    let data = req.body

    let taskID = data.TaskID;

    let editTask = `DELETE FROM TASKS WHERE TaskID = ${taskID}`;

    console.log(editTask)

    connection.query(editTask, (err, results) => {
        if (err) {
            res.send(err)
        }
        res.send(results)
    })

});

module.exports = router;