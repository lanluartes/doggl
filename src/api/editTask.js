var express = require('express');
var router = express.Router();
var db = require('../database/connection');

var connection = db.connection(); //connection to database

router.post('/editTask', (req, res) => {

    let data = req.body

    console.log(data)

    let taskID = data.TaskID;
    let title = data.TaskTitle;
    let isDone = data.isDone;
    let goal = data.TaskGoal;

    let editTask = `UPDATE TASKS SET TaskTitle = "${title}", isDone = ${isDone}, TaskGoal = "${goal}" WHERE TaskID = ${taskID}`;

    console.log(editTask)

    connection.query(editTask, (err, results) => {
        if (err) {
            res.send(err)
        }
        res.send(results)
    })

});

module.exports = router;