var express = require('express');
var router = express.Router();

var db = require('../database/connection');

var connection = db.connection(); //connection to database

var getTasks = `SELECT * FROM TASKS`;

router.get('/tasks', (req, res) => {
    connection.query(getTasks, (err, results) => {
        if (err) {
            res.send(err)
        }
        res.json(results)
    })
});

module.exports = router;