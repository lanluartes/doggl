import React from 'react';
import './assets/styles/scss/App.scss'
import Todo from './components/Todo'

function App() {
  return (
    // <Landing />
    <div>
      <Todo />
    </div>
  );
}

export default App;
