const express = require('express');
const bodyParser = require('body-parser');
const app = express();

var getTasks = require('./src/api/getTasks')
var addTask = require('./src/api/addTask')
var editTask = require('./src/api/editTask')
var deleteTask = require('./src/api/deleteTask')




app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/api', getTasks);
app.use('/api', addTask);
app.use('/api', editTask);
app.use('/api', deleteTask);

const port = 5000;

app.listen(port, console.log(`Server started on port ${port}`));